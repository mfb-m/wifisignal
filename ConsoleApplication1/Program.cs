﻿using NativeWifi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            WlanClient client = new WlanClient();
            foreach (WlanClient.WlanInterface wlanIface in client.Interfaces)
            {
                // Lists all available networks
                Wlan.WlanAvailableNetwork[] networks = wlanIface.GetAvailableNetworkList(0);
                client.Interfaces.FirstOrDefault().WlanConnectionNotification += Program_WlanConnectionNotification;
                client.Interfaces.FirstOrDefault().WlanNotification += Program_WlanNotification;
                client.Interfaces.FirstOrDefault().WlanReasonNotification += Program_WlanReasonNotification;
                foreach (Wlan.WlanAvailableNetwork network in networks)
                {
                    System.Console.WriteLine("Found network with SSID {0}, Quality {1}.", GetStringForSSID(network.dot11Ssid), network.wlanSignalQuality);
                }
                System.Console.ReadKey();
            }

        }

        private static void Program_WlanReasonNotification(Wlan.WlanNotificationData notifyData, Wlan.WlanReasonCode reasonCode)
        {
            throw new NotImplementedException();
        }

        private static void Program_WlanNotification(Wlan.WlanNotificationData notifyData)
        {
            var kk = notifyData.NotificationCode;
            Wlan.WlanAvailableNetwork[] networks;
            //networks.FirstOrDefault(o => o.);
        }

        private static void Program_WlanConnectionNotification(Wlan.WlanNotificationData notifyData, Wlan.WlanConnectionNotificationData connNotifyData)
        {
            var kk = notifyData.NotificationCode;
        }

        public static string GetStringForSSID(Wlan.Dot11Ssid ssid)
        {
            return Encoding.ASCII.GetString(ssid.SSID, 0, (int)ssid.SSIDLength);
        }
    }
}
